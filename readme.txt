These are examples of how to scrape data from websites. There is a 
progression to each file. Look at the files in this order to learn 
or if you are just looking for the end product jump to the last file. 

Source Reference:

http://brianabelson.com/open-news/2013/12/17/scrape-the-gibson.html


1. scraping_craigslist_printToScreen.py
The first file just accesses the website and prints to screen.

2. scraping_craigslist_return_list.py
The second file returns the data as a list of structs instead of printing
to the screen.

3. scraping_craigslist_db.py
The third file stores the data in a database (sql lite) instead of 
returning a list. 

4. scraping_craigslist_db_cache.py
store in a database and cache the webpage. 

5. scraping_craigslist_db_cache_threaded.py
Parallelizes (4) above by using threads. 

